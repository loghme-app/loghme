This code is the Backend of "Loghme", a food ordering application developed for online food ordering and delivery.

## To run the project with Docker

First you should run database:

### `docker build -t loghme-db .`

### `docker run -d --name Loghme-db -p 3303:3306 -e MYSQL_ROOT_PASSWORD=Loghme loghme-db`

And then you should enter:

### `docker build -t loghme-backend .`

### `docker run -t --name Loghme-backend -p 8080:8080 --link Loghme-db:db loghme-backend`
