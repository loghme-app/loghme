FROM maven:3.6.1-jdk-8 as maven_builder

WORKDIR /app

ADD pom.xml /app

RUN ["/usr/local/bin/mvn-entrypoint.sh", "mvn", "verify", "clean", "--fail-never"]

ADD . /app

RUN ["mvn","clean","install","-T","2C","-DskipTests=true"]

EXPOSE 8080

CMD mvn tomcat7:run

# FROM tomcat:9.0.31-jdk8

# COPY --from=maven_builder app/target/Loghme.war /usr/local/tomcat/webapps