CREATE SCHEMA IF NOT EXISTS `Loghme` DEFAULT CHARACTER SET utf8 COLLATE utf8_persian_ci ;

CREATE TABLE `Loghme`.`Restaurant` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(70) NOT NULL,
  `location_x` INT NULL,
  `location_y` INT NULL,
  `logo` VARCHAR(200) NULL,
  PRIMARY KEY (`id`),
  UNIQUE (`name`)
  );

CREATE TABLE `Loghme`.`Food` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `description` VARCHAR(300) NULL,
  `popularity` DECIMAL(3,2) NULL,
  `price` INT NULL,
  `image` VARCHAR(200) NULL,
  `restaurant_name` VARCHAR(70) NULL,
  `restaurant` INT UNSIGNED NULL,
  PRIMARY KEY (`id`),
  INDEX `restaurant_idx` (`restaurant` ASC) VISIBLE,
  CONSTRAINT `food_restaurant`
    FOREIGN KEY (`restaurant`)
    REFERENCES `Loghme`.`Restaurant` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `Loghme`.`DiscountFood` (
  `old_price` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `food` int(10) unsigned NOT NULL,
  `valid` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`food`),
  KEY `discountFood_food_idx` (`food`),
  CONSTRAINT `discountFood_food` 
	FOREIGN KEY (`food`) 
    REFERENCES `Loghme`.`Food` (`id`) 
    ON DELETE CASCADE
);

CREATE TABLE `Loghme`.`User` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `credit` int(11) DEFAULT NULL,
  `location_x` int(11) DEFAULT NULL,
  `location_y` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
);
  
CREATE TABLE `Loghme`.`Order` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `price` INT NULL,
  `status` VARCHAR(20) NULL,
  `restaurant_name` VARCHAR(70) NULL,
  `user` INT UNSIGNED NULL,  
  PRIMARY KEY (`id`),
  INDEX `user_idx` (`user` ASC) VISIBLE,
  CONSTRAINT `order_user`
    FOREIGN KEY (`user`)
    REFERENCES `Loghme`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
CREATE TABLE `Loghme`.`Delivery` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `velocity` INT NULL,
  `location_x` INT NULL,
  `location_y` INT NULL,
   `delivery_time` INT NULL,
  `start_delivery_time` DATETIME NULL,
  `order` INT UNSIGNED NULL,
  PRIMARY KEY (`id`),
  INDEX `order_idx` (`order` ASC) VISIBLE,
  CONSTRAINT `delivery_order`
    FOREIGN KEY (`order`)
    REFERENCES `Loghme`.`Order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `Loghme`.`OrderItem` (
  `order_id` INT UNSIGNED NOT NULL,
  `food_id` INT UNSIGNED NOT NULL,
  `count` INT NULL,
  PRIMARY KEY (`order_id`, `food_id`),
  INDEX `orderItem_food_idx` (`food_id` ASC) VISIBLE,
  CONSTRAINT `orderItem_order`
    FOREIGN KEY (`order_id`)
    REFERENCES `Loghme`.`Order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `orderItem_food`
    FOREIGN KEY (`food_id`)
    REFERENCES `Loghme`.`Food` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

