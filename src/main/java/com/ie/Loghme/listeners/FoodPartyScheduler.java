package com.ie.Loghme.listeners;

import com.ie.Loghme.services.FoodPartyService;
import com.ie.Loghme.services.LoghmeConnectionService;
import com.ie.Loghme.services.UserService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@WebListener
public class FoodPartyScheduler implements ServletContextListener {
    private ScheduledExecutorService scheduler;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        scheduler = Executors.newSingleThreadScheduledExecutor();
        // *** MAKE SURE TO ALSO CHANGE THE DURATION IN FoodPartyView.java *** //
        scheduler.scheduleAtFixedRate(new FoodPartyUpdater(), 0, 2, TimeUnit.MINUTES);
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        scheduler.shutdownNow();
    }

    private static class FoodPartyUpdater implements Runnable {
        @Override
        public void run() {
//            UserService.getInstance().removeExpiredDiscountFoodsFromUserCarts();
            FoodPartyService.getInstance().removeExpiredDiscountFoodsFromCarts();
            LoghmeConnectionService.getInstance().getFoodPartyData();
            FoodPartyService.getInstance().startNewOffer();
        }
    }
}
