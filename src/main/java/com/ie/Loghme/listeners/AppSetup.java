package com.ie.Loghme.listeners;

import com.ie.Loghme.mappers.UserMapper;
import com.ie.Loghme.services.LoghmeConnectionService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


public class AppSetup implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LoghmeConnectionService.getInstance().getRestaurantsData();

        UserMapper.getInstance().insert();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
