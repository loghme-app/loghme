package com.ie.Loghme.exceptions;

public class FoodNotFound extends Exception {
    public FoodNotFound() {
        super("FOOD_NOT_FOUND");
    }
}
