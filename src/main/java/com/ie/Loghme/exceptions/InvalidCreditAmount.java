package com.ie.Loghme.exceptions;

public class InvalidCreditAmount extends Exception {
    public InvalidCreditAmount() {
        super("INVALID_CREDIT_AMOUNT");
    }
}
