package com.ie.Loghme.exceptions;

public class InvalidLoginInfo  extends Exception {
    public InvalidLoginInfo() {
        super("INVALID_LOGIN_INFO");
    }
}
