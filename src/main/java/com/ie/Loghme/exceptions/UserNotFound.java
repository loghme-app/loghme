package com.ie.Loghme.exceptions;

public class UserNotFound extends Exception {
    public UserNotFound() {
        super("USER_NOT_FOUND");
    }
}
