package com.ie.Loghme.exceptions;

public class InvalidGoogleLoginInfo extends Exception {
    public InvalidGoogleLoginInfo() {
        super("INVALID_GOOGLE_LOGIN_INFO");
    }
}
