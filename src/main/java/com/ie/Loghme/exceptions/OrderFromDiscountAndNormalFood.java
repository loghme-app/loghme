package com.ie.Loghme.exceptions;

public class OrderFromDiscountAndNormalFood extends Exception {
    public OrderFromDiscountAndNormalFood() {
        super("ORDER_FROM_DISCOUNT_AND_NORMAL_FOOD");
    }
}
