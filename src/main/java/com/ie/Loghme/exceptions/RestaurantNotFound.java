package com.ie.Loghme.exceptions;

public class RestaurantNotFound extends Exception {
    public RestaurantNotFound() {
        super("RESTAURANT_NOT_FOUND");
    }
}
