package com.ie.Loghme.exceptions;

public class InvalidEmailAddress extends Exception {
    public InvalidEmailAddress() {
        super("INVALID_EMAIL_ADDRESS");
    }
}
