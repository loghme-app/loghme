package com.ie.Loghme.exceptions;

public class OrderNotFound extends Exception {
    public OrderNotFound() {
        super("ORDER_NOT_FOUND");
    }
}
