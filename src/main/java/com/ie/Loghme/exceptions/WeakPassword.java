package com.ie.Loghme.exceptions;

public class WeakPassword extends Exception {
    public WeakPassword() {
        super("WEAK_PASSWORD");
    }
}
