package com.ie.Loghme.exceptions;

public class NoItemInCartToFinalize extends Exception {
    public NoItemInCartToFinalize() {
        super("NO_ITEM_IN_CART");
    }
}
