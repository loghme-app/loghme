package com.ie.Loghme.exceptions;

public class RestaurantOutOfReach extends Exception {
    public RestaurantOutOfReach() {
        super("RESTAURANT_OUT_OF_REACH");
    }
}
