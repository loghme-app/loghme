package com.ie.Loghme.exceptions;

public class FoodItemNotInCart extends Exception {
    public FoodItemNotInCart() {
        super("FOOD_ITEM_NOT_IN_CART");
    }
}
