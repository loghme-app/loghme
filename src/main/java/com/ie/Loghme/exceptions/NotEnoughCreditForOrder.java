package com.ie.Loghme.exceptions;

public class NotEnoughCreditForOrder extends Exception {
    public NotEnoughCreditForOrder() {
        super("NOT_ENOUGH_CREDIT");
    }
}
