package com.ie.Loghme.exceptions;

public class EmailAlreadyExists extends Exception {
    public EmailAlreadyExists() {
        super("EMAIL_ALREADY_EXISTS");
    }
}