package com.ie.Loghme.exceptions;

public class OrderFromMultipleRestaurants extends Exception {
    public OrderFromMultipleRestaurants() {
        super("ORDER_FROM_MULTIPLE_RESTAURANTS");
    }
}
