package com.ie.Loghme.exceptions;

public class FoodOutOfStock extends Exception {
    public FoodOutOfStock() {
        super("FOOD_OUT_OF_STOCK");
    }
}
