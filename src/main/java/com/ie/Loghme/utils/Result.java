package com.ie.Loghme.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class Result {
    private boolean success;
    private String error;

    public Result(boolean success, String error) {
        this.success = success;
        this.error = error;
    }

    public Result(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public byte[] serialize() throws IOException {
        return new ObjectMapper().writeValueAsString(this).getBytes();
    }
}
