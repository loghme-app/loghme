package com.ie.Loghme.utils;

public class JwtToken {
    private String jwt;

    public JwtToken(String jwt) {
        this.jwt = jwt;
    }

    public String getJwt() { return jwt; }
}
