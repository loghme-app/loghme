package com.ie.Loghme.utils;

import com.ie.Loghme.models.Location;
import com.ie.Loghme.models.User;

public class LocationUtils {
    public Double calculateDistance(Location start, Location end){
        return Math.sqrt(Math.pow((end.getX() - start.getX()),2) + Math.pow(end.getY() - start.getY(), 2));
    }
}
