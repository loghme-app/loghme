package com.ie.Loghme.utils;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.ie.Loghme.exceptions.InvalidGoogleLoginInfo;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

public class GoogleTokenVerifier {
    private static GoogleTokenVerifier instance = null;
    private final String CLIENT_ID = "867238827080-d0eljctvi3lgjoen339p5njp9h8t9tv8.apps.googleusercontent.com";

    private GoogleTokenVerifier(){}

    public static GoogleTokenVerifier getInstance(){
        if(instance == null)
            instance = new GoogleTokenVerifier();
        return instance;
    }

    public String getEmail(String token) throws InvalidGoogleLoginInfo {
        JacksonFactory jacksonFactory = new JacksonFactory();
        NetHttpTransport netHttpTransport = new NetHttpTransport();
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(netHttpTransport, jacksonFactory)
                .setAudience(Collections.singletonList(this.CLIENT_ID))
                .build();
        try{
            GoogleIdToken idToken = verifier.verify(token);
            if (idToken == null) {
                throw new InvalidGoogleLoginInfo();
            }
            Payload payload = idToken.getPayload();
            return payload.getEmail();
        }
        catch(GeneralSecurityException | IOException ex){
            ex.printStackTrace();
            throw new InvalidGoogleLoginInfo();
        }
    }
}
