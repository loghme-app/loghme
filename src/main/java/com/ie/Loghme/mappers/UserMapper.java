package com.ie.Loghme.mappers;

import com.ie.Loghme.exceptions.EmailAlreadyExists;
import com.ie.Loghme.exceptions.UserNotFound;
import com.ie.Loghme.models.*;

import java.sql.*;

public class UserMapper {
    private static UserMapper instance = null;

    public static UserMapper getInstance(){
        if(instance == null)
            instance = new UserMapper();
        return instance;
    }

    private UserMapper(){}

    private static final String FIND_COLUMNS = "id, firstname, lastname, phone, email, credit, location_x, location_y, password";
    private static final String INSERT_COLUMNS = "firstname, lastname, phone, email, credit, location_x, location_y, password";
    private static final String TABLE_NAME = "User";

    //TODO: remove when multi-user
    public void insert() {
        try{
            this.insert(new User(1,"احسان", "خامس پناه", "۰۹۱۲۴۸۲۰۱۹۴",
                    "ekhamespanah@yahoo.com", 0, new Location(0,0), "1234") );
        }
        catch(EmailAlreadyExists ex){
        }
    }

    // TODO :remove when multi-user
    public User find() {
        return this.find(1);
    }

    public User find(Integer userId) {
        try{
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getFindStatement(con, userId);
            ResultSet rs = st.executeQuery();
            rs.next();
            User user = convertResultSetToObject(rs);
            rs.close();
            st.close();
            con.close();
            return user;
        }
        catch(SQLException ex){
            ex.printStackTrace();
            return null;
        }
    }

    public User findByEmail(String email)throws UserNotFound{
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getFindByEmailStatement(con, email);
            ResultSet rs = st.executeQuery();
            if(!rs.next())
                throw new UserNotFound();
            User user = convertResultSetToObject(rs);
            rs.close();
            st.close();
            con.close();
            return user;
        }
        catch(SQLException ex){
            ex.printStackTrace();
            return null;
        }
    }

    public void insert(User user) throws EmailAlreadyExists {
        try{
            Integer userId = 0;

            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getInsertStatement(con, user);
            st.executeUpdate();
            ResultSet result = st.getGeneratedKeys();
            if(result.next()) {
                userId = result.getInt(1);
            }
            CartMapper.getInstance().insert(new Cart(userId));

            result.close();
            st.close();
            con.close();
        }
        catch(SQLIntegrityConstraintViolationException ex){
            throw new EmailAlreadyExists();
        }
        catch(SQLException ex){
            ex.printStackTrace();
        }
    }

    public void update(User user){
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getUpdateStatement(con, user);
            st.executeUpdate();
            st.close();
            con.close();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    private PreparedStatement getFindStatement(Connection con, Integer id) throws SQLException{
        String query = String.format(
                "SELECT %s FROM %s WHERE id = ?",
                FIND_COLUMNS,
                TABLE_NAME
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, id);

        return stmt;
    }

    private PreparedStatement getFindByEmailStatement(Connection con, String email) throws SQLException {
        String query = String.format(
                "SELECT %s FROM %s WHERE email = ?",
                FIND_COLUMNS,
                TABLE_NAME
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setString(1, email);

        return stmt;
    }

    private PreparedStatement getInsertStatement(Connection con, User user) throws SQLException {
        String query = String.format(
                "INSERT INTO %s(%s) VALUES(?,?,?,?,?,?,?,?)",
                TABLE_NAME,
                INSERT_COLUMNS
        );

        PreparedStatement stmt = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, user.getFirstName());
        stmt.setString(2, user.getLastName());
        stmt.setString(3, user.getTelephoneNumber());
        stmt.setString(4, user.getEmail());
        stmt.setInt(5, user.getCredit());
        stmt.setInt(6, user.getLocation().getX());
        stmt.setInt(7, user.getLocation().getY());
        stmt.setString(8,  user.getPassword());

        return stmt;
    }

    private PreparedStatement getUpdateStatement(Connection con, User user) throws SQLException {
        String query = String.format(
                "UPDATE %s SET credit = ? WHERE id = ?",
                TABLE_NAME
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, user.getCredit());
        stmt.setInt(2,  user.getId());

        return stmt;
    }

    private User convertResultSetToObject(ResultSet resultSet) throws SQLException {
        Integer id = resultSet.getInt("id");
        String firstName = resultSet.getString("firstname");
        String lastName = resultSet.getString("lastname");
        String phoneNumber = resultSet.getString("phone");
        String email = resultSet.getString("email");
        Integer credit = resultSet.getInt("credit");
        Integer X = resultSet.getInt("location_x");
        Integer Y = resultSet.getInt("location_y");
        String password = resultSet.getString("password");
        return new User(id, firstName, lastName, phoneNumber, email, credit, new Location(X,Y), password);
    }
}
