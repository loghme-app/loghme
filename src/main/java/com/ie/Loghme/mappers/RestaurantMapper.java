package com.ie.Loghme.mappers;

import com.ie.Loghme.exceptions.RestaurantNotFound;
import com.ie.Loghme.models.Food;
import com.ie.Loghme.models.Location;
import com.ie.Loghme.models.Restaurant;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Statement;


public class RestaurantMapper {
    private static RestaurantMapper instance = null;

    public static RestaurantMapper getInstance(){
        if(instance == null)
            instance = new RestaurantMapper();
        return instance;
    }

    private RestaurantMapper(){}

    private static final String FIND_COLUMNS = "id, name, location_x, location_y, logo";
    private static final String INSERT_COLUMNS = "name, location_x, location_y, logo";
    private static final String TABLE_NAME = "Restaurant";

    public ArrayList<Restaurant> search(String restaurantKeyword, String foodKeyword, Location userLocation) {
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getSearchStatement(con, restaurantKeyword, foodKeyword, userLocation);
            ResultSet rs = st.executeQuery();
            ArrayList<Restaurant> restaurants = new ArrayList<>();
            while (rs.next()) {
                restaurants.add(convertResultSetToObject(rs, FoodMapper.getInstance().findAll(rs.getInt("id"))));
            }
            rs.close();
            st.close();
            con.close();
            return restaurants;
        }
        catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    private PreparedStatement getSearchStatement(Connection con, String restaurantKeyword, String foodKeyword, Location userLocation)
            throws SQLException {
        String query = String.format(
                "SELECT %s FROM Restaurant R " +
                "WHERE SQRT( POW((? - location_x), 2) + POW((? - location_y), 2) ) <= 170 " +
                "AND R.name LIKE ? " +
                "AND R.id IN (SELECT R2.id FROM Restaurant R2 JOIN Food F ON R2.id = F.restaurant WHERE F.name LIKE ?)",
                FIND_COLUMNS
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, userLocation.getX());
        stmt.setInt(2, userLocation.getY());
        stmt.setString(3, "%%" + restaurantKeyword + "%%");
        stmt.setString(4, "%%" + foodKeyword + "%%");

        return stmt;
    }

    public Restaurant find(Integer restaurantId) throws RestaurantNotFound {
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getFindStatement(con, restaurantId);
            ResultSet rs = st.executeQuery();
            if(! rs.next())
                throw new RestaurantNotFound();
            Restaurant restaurant = convertResultSetToObject(rs, FoodMapper.getInstance().findAll(restaurantId));
            rs.close();
            st.close();
            con.close();
            return restaurant;
        }
        catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Restaurant findByName(String restaurantName) {
        Restaurant restaurant = null;
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getFindStatement(con, restaurantName);
            ResultSet rs = st.executeQuery();
            rs.next();
            restaurant = convertResultSetToObject(rs, FoodMapper.getInstance().findAll(rs.getInt("id")));
            rs.close();
            st.close();
            con.close();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return restaurant;
    }

    public ArrayList<Restaurant> findAll(Location userLocation, Integer pageNum, Integer itemsPerPage) {
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getFindAllStatement(con, userLocation, pageNum, itemsPerPage);
            ResultSet rs = st.executeQuery();
            ArrayList<Restaurant> restaurants = new ArrayList<>();
            while (rs.next()) {
                restaurants.add(convertResultSetToObject(rs, FoodMapper.getInstance().findAll(rs.getInt("id"))));
            }
            rs.close();
            st.close();
            con.close();
            return restaurants;
        }
        catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    public Integer insert(Restaurant restaurant){
        Integer restaurantId = -1;
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getInsertStatement(con, restaurant);
            st.executeUpdate();
            ResultSet result = st.getGeneratedKeys();
            if (result.next()) {
                restaurantId = result.getInt(1);

                FoodMapper.getInstance().insertAll(restaurant.getMenu(), restaurantId);
            }
            result.close();
            st.close();
            con.close();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return restaurantId;
    }

    private PreparedStatement getFindStatement(Connection con, Integer id) throws SQLException {
        String query = String.format(
                "SELECT %s FROM %s WHERE id = ?",
                FIND_COLUMNS,
                TABLE_NAME
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, id);

        return stmt;
    }

    private PreparedStatement getFindStatement(Connection con, String name) throws SQLException{
       String query = String.format(
                "SELECT %s FROM %s WHERE name = ?",
                FIND_COLUMNS,
                TABLE_NAME
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setString(1, name);

        return stmt;
    }

    private PreparedStatement getFindAllStatement(Connection con, Location userLocation, Integer pageNum, Integer itemsPerPage)
            throws SQLException{
        String query = String.format(
                "SELECT %s FROM %s WHERE SQRT( POW((? - location_x), 2) + POW((? - location_y), 2) ) <= 170 LIMIT ? OFFSET ? ",
                FIND_COLUMNS,
                TABLE_NAME
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, userLocation.getX());
        stmt.setInt(2, userLocation.getY());
        stmt.setInt(3, itemsPerPage);
        stmt.setInt(4, (pageNum-1)*itemsPerPage);

        return stmt;
    }

    private PreparedStatement getInsertStatement(Connection con, Restaurant restaurant) throws SQLException{
        String query = String.format(
                "INSERT IGNORE INTO %s(%s) VALUES(?,?,?,?)",
                TABLE_NAME,
                INSERT_COLUMNS
        );

        PreparedStatement stmt = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, restaurant.getName());
        stmt.setInt(2, restaurant.getLocation().getX());
        stmt.setInt(3, restaurant.getLocation().getY());
        stmt.setString(4, restaurant.getLogoURL());

        return stmt;
    }

    private Restaurant convertResultSetToObject(ResultSet resultSet, ArrayList<Food> menu) throws SQLException {
        Integer id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        Integer X = resultSet.getInt("location_x");
        Integer Y = resultSet.getInt("location_y");
        String logoURL = resultSet.getString("logo");
        Restaurant restaurant = new Restaurant(name, new Location(X,Y), logoURL, menu);
        restaurant.setId(id);
        return restaurant;
    }
}