package com.ie.Loghme.mappers;

import com.ie.Loghme.exceptions.OrderNotFound;
import com.ie.Loghme.models.FoodItem;
import com.ie.Loghme.models.Order;

import java.sql.*;
import java.util.ArrayList;

public class OrderMapper {
    private static OrderMapper instance = null;

    public static OrderMapper getInstance(){
        if(instance == null)
            instance = new OrderMapper();
        return instance;
    }

    private OrderMapper(){}

    private static final String TABLE_NAME = "Loghme.Order";
    private static final String INSERT_COLUMNS = "price, status, restaurant_name, user";
    private static final String FIND_COLUMNS = "id, price, status, restaurant_name, user";

    public Integer insert(Order order) {
        Integer orderId = 0;
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getInsertStatement(con, order);
            st.executeUpdate();

            ResultSet result = st.getGeneratedKeys();
            if (result.next()) {
                orderId = result.getInt(1);
                OrderItemMapper.getInstance().insertAll(order.getFoods(), orderId);
            }

            st.close();
            con.close();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return orderId;
    }

    private PreparedStatement getInsertStatement(Connection con, Order order) throws SQLException {
        String query = String.format(
                "INSERT INTO %s (%s) VALUES (?,?,?,?)",
                TABLE_NAME,
                INSERT_COLUMNS
        );

        PreparedStatement stmt = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        stmt.setInt(1,  order.getPrice());
        stmt.setString(2, order.getStatus().toString());
        stmt.setString(3, order.getRestaurantName());
        stmt.setInt(4, order.getUserId());

        return stmt;
    }

    public void update(Order order) {
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getUpdateStatement(con, order);
            st.executeUpdate();
            st.close();
            con.close();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    private PreparedStatement getUpdateStatement(Connection con, Order order) throws SQLException {
        String query = String.format(
                "UPDATE %s SET status=? WHERE id=?",
                TABLE_NAME
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setString(1, order.getStatus().toString());
        stmt.setInt(2, order.getId());

        return stmt;
    }

    public Order find(Integer userId, Integer orderId) throws OrderNotFound {
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getFindStatement(con, userId, orderId);
            ResultSet rs = st.executeQuery();
            if(!rs.next())
                throw new OrderNotFound();
            Integer id = rs.getInt("id");
            Order order = convertResultSetToObject(rs, OrderItemMapper.getInstance().findAll(id));
            rs.close();
            st.close();
            con.close();
            return order;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    private PreparedStatement getFindStatement(Connection con, Integer userId, Integer id) throws SQLException{
        String query = String.format(
                "SELECT %s FROM %s WHERE id=? AND user=?",
                FIND_COLUMNS,
                TABLE_NAME
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, id);
        stmt.setInt(2, userId);

        return stmt;
    }

    public ArrayList<Order> findAll(Integer userId) {
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getFindAllStatement(con, userId);
            ResultSet rs = st.executeQuery();
            ArrayList<Order> orders = new ArrayList<>();
            while (rs.next()) {
                Integer id = rs.getInt("id");
                orders.add(convertResultSetToObject(rs, OrderItemMapper.getInstance().findAll(id)));
            }
            rs.close();
            st.close();
            con.close();
            return orders;
        }
        catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    private PreparedStatement getFindAllStatement(Connection con, Integer userId) throws SQLException{
        String query = String.format(
                "SELECT %s FROM %s WHERE user = ?",
                FIND_COLUMNS,
                TABLE_NAME
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, userId);

        return stmt;
    }

    private Order convertResultSetToObject(ResultSet resultSet, ArrayList<FoodItem> items) throws SQLException {
        Integer id = resultSet.getInt("id");
        Integer price = resultSet.getInt("price");
        String status = resultSet.getString("status");
        String restaurantName = resultSet.getString("restaurant_name");
        Integer userId = resultSet.getInt("user");

        return new Order(id, price, status, restaurantName, userId, items);
    }
}
