package com.ie.Loghme.mappers;

import com.ie.Loghme.exceptions.FoodNotFound;
import com.ie.Loghme.exceptions.RestaurantNotFound;
import com.ie.Loghme.models.DiscountFood;
import com.ie.Loghme.models.Restaurant;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DiscountFoodMapper {
    private static DiscountFoodMapper instance = null;

    public static DiscountFoodMapper getInstance() {
        if (instance == null)
            instance = new DiscountFoodMapper();
        return instance;
    }

    private DiscountFoodMapper() {}

    private static final String TABLE_NAME = "DiscountFood";
    private static final String INSERT_COLUMNS = "old_price, count, food, valid";
    private static final String FIND_COLUMNS = "old_price, count, food";

    public Integer findCount(Integer discountFoodId){
        int count = 0;
        try{
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getFindCountStatement(con, discountFoodId);
            ResultSet rs = st.executeQuery();
            rs.next();
            count = rs.getInt("count");
            st.close();
            con.close();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return count;
    }

    private PreparedStatement getFindCountStatement(Connection con, Integer id) throws SQLException{
        String query =  String.format(
                "SELECT %s FROM %s WHERE food = ?",
                "count",
                TABLE_NAME
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, id);

        return stmt;
    }

    public DiscountFood find(Integer discountFoodId) throws FoodNotFound {
        DiscountFood food = null;
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getFindStatement(con, discountFoodId);
            ResultSet rs = st.executeQuery();
            if(!rs.next())
                throw new FoodNotFound();
            PreparedStatement fst = FoodMapper.getInstance().getFindStatement(con, discountFoodId);
            ResultSet foodrs = fst.executeQuery();
            foodrs.next();
            Integer restaurantId = foodrs.getInt("restaurant");
            Restaurant restaurant = RestaurantMapper.getInstance().find(restaurantId);
            food = convertResultSetToObject(rs, foodrs, restaurant);
            rs.close();
            st.close();
            con.close();
        }
        catch(RestaurantNotFound | SQLException e){
            e.printStackTrace();
        }
        return food;
    }

    private PreparedStatement getFindStatement(Connection con, Integer id) throws SQLException{
        String query = String.format(
                "SELECT %s FROM %s WHERE food=? and valid=1",
                FIND_COLUMNS,
                TABLE_NAME
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, id);

        return stmt;
    }

    public ArrayList<DiscountFood> findAll() {
        ArrayList<DiscountFood> discountFoods = new ArrayList<>();
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getFindAllStatement(con);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                try {
                    Integer foodId = rs.getInt("food");
                    PreparedStatement fst = FoodMapper.getInstance().getFindStatement(con, foodId);
                    ResultSet foodrs = fst.executeQuery();
                    foodrs.next();
                    Integer restaurantId = foodrs.getInt("restaurant");
                    Restaurant restaurant = RestaurantMapper.getInstance().find(restaurantId);
                    discountFoods.add(convertResultSetToObject(rs, foodrs, restaurant));
                } catch (RestaurantNotFound e) {
                    e.printStackTrace();
                }
            }
            rs.close();
            st.close();
            con.close();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return discountFoods;
    }

    private PreparedStatement getFindAllStatement(Connection con) throws SQLException{
        String query = String.format(
                "SELECT %s FROM %s WHERE valid=1",
                FIND_COLUMNS,
                TABLE_NAME
        );

        return con.prepareStatement(query);
    }

    public void insertAll(List<DiscountFood> discountFoods) throws SQLException{
        Connection con = ConnectionPool.getConnection();
        for(DiscountFood discountFood : discountFoods){

            PreparedStatement foodst = FoodMapper.getInstance().getInsertStatement(con,
                    discountFood, discountFood.getRestaurant().getId());
            foodst.executeUpdate();
            ResultSet result = foodst.getGeneratedKeys();
            if(result.next()) {
                Integer foodId = result.getInt(1);
                PreparedStatement st = getInsertStatement(con, discountFood, foodId);
                st.executeUpdate();
                st.close();
            }
            foodst.close();
            result.close();
        }
        con.close();
    }

    private PreparedStatement getInsertStatement(Connection con, DiscountFood dfood, Integer foodId) throws SQLException{
        String query = String.format(
                "INSERT INTO %s (%s) VALUES (?,?,?,?)",
                TABLE_NAME,
                INSERT_COLUMNS
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, dfood.getOldPrice());
        stmt.setInt(2, dfood.getCount());
        stmt.setInt(3, foodId);
        stmt.setInt(4, 1);

        return stmt;
    }

    public void update(DiscountFood dFood){
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getUpdateStatement(con, dFood);
            st.executeUpdate();
            st.close();
            con.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private PreparedStatement getUpdateStatement(Connection con, DiscountFood dFood) throws SQLException{
        String query =  String.format(
                "UPDATE %s SET count=? WHERE food=?",
                TABLE_NAME
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, dFood.getCount());
        stmt.setInt(2, dFood.getId());

        return stmt;
    }

    public void updateAll() throws SQLException{
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = getUpdateAllStatement(con);
        st.executeUpdate();
        st.close();
        con.close();
    }

    private PreparedStatement getUpdateAllStatement(Connection con) throws SQLException{
        String query = String.format(
                "UPDATE %s SET valid=0",
                TABLE_NAME
        );

        return con.prepareStatement(query);
    }

    private DiscountFood convertResultSetToObject(ResultSet resultSet, ResultSet foodRes, Restaurant restaurant) throws SQLException {
        Integer id = resultSet.getInt("food");
        Integer oldPrice = resultSet.getInt("old_price");
        Integer count = resultSet.getInt("count");

        String name = foodRes.getString("name");
        String description = foodRes.getString("description");
        Double popularity = foodRes.getDouble("popularity");
        Integer price = foodRes.getInt("price");
        String imageURL = foodRes.getString("image");
        String restaurantName = foodRes.getString("restaurant_name");

        DiscountFood food = new DiscountFood(name, description, popularity, price, imageURL, restaurantName, oldPrice, count, restaurant);
        food.setId(id);
        return food;
    }
}
