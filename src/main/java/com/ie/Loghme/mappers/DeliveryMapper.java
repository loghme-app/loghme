package com.ie.Loghme.mappers;

import com.ie.Loghme.models.Delivery;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.Instant;

public class DeliveryMapper {
    private static DeliveryMapper instance = null;

    public static DeliveryMapper getInstance(){
        if(instance == null)
            instance = new DeliveryMapper();
        return instance;
    }

    private DeliveryMapper(){}

    private static final String INSERT_COLUMNS = "velocity, location_x, location_y, delivery_time, start_delivery_time, `order`";
    private static final String TABLE_NAME = "Delivery";

    public void insert(Delivery delivery, Integer orderId, Integer deliveryTime, Instant startDeliveryTime){
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getInsertStatement(con, delivery, orderId, deliveryTime, startDeliveryTime);
            st.executeUpdate();
            st.close();
            con.close();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    private PreparedStatement getInsertStatement(Connection con, Delivery delivery, Integer orderId, Integer deliveryTime, Instant startDeliveryTime) throws SQLException{
        Date date = new Date(Timestamp.from(startDeliveryTime).getTime());
        SimpleDateFormat format = new  SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
        String startDateTime = format.format(date);

        String query = String.format(
                "INSERT INTO %s (%s) VALUES (?,?,?,?,?,?)",
                TABLE_NAME,
                INSERT_COLUMNS
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, delivery.getVelocity());
        stmt.setInt(2, delivery.getLocation().getX());
        stmt.setInt(3, delivery.getLocation().getY());
        stmt.setInt(4, deliveryTime);
        stmt.setString(5, startDateTime);
        stmt.setInt(6, orderId);

        return stmt;
    }
}
