package com.ie.Loghme.mappers;

import com.ie.Loghme.models.Cart;

import java.util.ArrayList;

public class CartMapper {
    private static CartMapper instance = null;

    private ArrayList<Cart> carts;

    public static CartMapper getInstance(){
        if(instance == null)
            instance = new CartMapper();
        return instance;
    }

    private CartMapper(){
        carts = new ArrayList<>();
    }

    public void insert(Cart cart){
        carts.add(cart);
    }

    public Cart find(Integer userId){
        for(Cart cart: carts){
            if(cart.getUserId().equals(userId)){
                return cart;
            }
        }

        Cart newCart = new Cart(userId);
        this.insert(newCart);
        return newCart;
    }

    public ArrayList<Cart> findAll(){
        return carts;
    }
}
