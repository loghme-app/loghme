package com.ie.Loghme.mappers;


import com.ie.Loghme.exceptions.FoodNotFound;
import com.ie.Loghme.models.Food;

import java.sql.*;
import java.util.ArrayList;

public class FoodMapper {
    private static FoodMapper instance = null;

    public static FoodMapper getInstance(){
        if(instance == null)
            instance = new FoodMapper();
        return instance;
    }

    private FoodMapper(){}

    private static final String INSERT_COLUMNS = "name, description, popularity, price, image, restaurant_name, restaurant";
    private static final String FIND_COLUMNS = "id, name, description, popularity, price, image, restaurant_name, restaurant";
    private static final String TABLE_NAME = "Food";

    public Food find(Integer foodId){
        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement st = getFindStatement(con, foodId);
            ResultSet rs = st.executeQuery();
            rs.next();
            Food food = convertResultSetToObject(rs);
            rs.close();
            st.close();
            con.close();
            return food;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<Food> findAll(Integer restaurantId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = getFindAllStatement(con, restaurantId);
        ResultSet rs = st.executeQuery();
        ArrayList<Food> foods = new ArrayList<>();
        while(rs.next()){
            foods.add(convertResultSetToObject(rs));
        }
        rs.close();
        st.close();
        con.close();
        return foods;
    }

    public void insertAll(ArrayList<Food> menu, Integer restaurantId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        for(Food food: menu){
            PreparedStatement st = getInsertStatement(con, food, restaurantId);
            st.executeUpdate();
            st.close();
        }
        con.close();
    }

    protected PreparedStatement getFindStatement(Connection con, Integer id) throws SQLException{
        String query = String.format(
                "SELECT %s FROM %s WHERE id = ?",
                FIND_COLUMNS,
                TABLE_NAME
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, id);

        return stmt;
    }

    private PreparedStatement getFindAllStatement(Connection con, Integer restaurantId) throws SQLException{
        String query = String.format(
                "SELECT %s FROM %s WHERE restaurant = ?",
                FIND_COLUMNS,
                TABLE_NAME
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1, restaurantId);

        return stmt;
    }

    protected PreparedStatement getInsertStatement(Connection con, Food food, Integer restaurantId) throws SQLException{
        String query = String.format(
                "INSERT INTO %s (%s) VALUES (?,?,?,?,?,?,?)",
                TABLE_NAME,
                INSERT_COLUMNS
        );

        PreparedStatement stmt = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, food.getName());
        stmt.setString(2, food.getDescription());
        stmt.setDouble(3, food.getPopularity());
        stmt.setInt(4, food.getPrice());
        stmt.setString(5, food.getImageURL());
        stmt.setString(6, food.getRestaurantName());
        stmt.setInt(7, restaurantId);

        return stmt;
    }

    private Food convertResultSetToObject(ResultSet resultSet) throws SQLException {
        Integer id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        String description = resultSet.getString("description");
        Double popularity = resultSet.getDouble("popularity");
        Integer price = resultSet.getInt("price");
        String imageURL = resultSet.getString("image");
        String restaurantName = resultSet.getString("restaurant_name");
        Food food = new Food(name, description, popularity, price, imageURL, restaurantName);
        food.setId(id);
        return food;
    }
}
