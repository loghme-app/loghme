package com.ie.Loghme.mappers;

import com.ie.Loghme.exceptions.FoodNotFound;
import com.ie.Loghme.models.Food;
import com.ie.Loghme.models.FoodItem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class OrderItemMapper {
    private static OrderItemMapper instance = null;

    public static OrderItemMapper getInstance(){
        if(instance == null)
            instance = new OrderItemMapper();
        return instance;
    }

    private OrderItemMapper(){}

    private static final String INSERT_COLUMNS = "order_id, food_id, count";
    private static final String FIND_COLUMNS = "food_id, count";
    private static final String TABLE_NAME = "OrderItem";

    public ArrayList<FoodItem> findAll(Integer orderId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement st = getFindAllStatement(con, orderId);
        ResultSet rs = st.executeQuery();
        ArrayList<FoodItem> foodItems = new ArrayList<>();
        while(rs.next()){
                foodItems.add(convertResultSetToObject(rs, FoodMapper.getInstance().find(rs.getInt("food_id"))));
        }
        rs.close();
        st.close();
        con.close();
        return foodItems;
    }

    public void insertAll(ArrayList<FoodItem> orderItems, Integer orderId) throws SQLException {
        Connection con = ConnectionPool.getConnection();
        for(FoodItem orderItem: orderItems){
            PreparedStatement st = getInsertStatement(con, orderItem, orderId);
            st.executeUpdate();
            st.close();
        }
        con.close();
    }

    private PreparedStatement getFindAllStatement(Connection con, Integer orderId) throws SQLException {
        String query = String.format(
                "SELECT %s FROM %s WHERE order_id = ?",
                FIND_COLUMNS,
                TABLE_NAME
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1,  orderId);

        return stmt;
    }

    private PreparedStatement getInsertStatement(Connection con, FoodItem orderItem, Integer orderId) throws SQLException{
        String query = String.format(
                "INSERT INTO %s (%s) VALUES (?,?,?)",
                TABLE_NAME,
                INSERT_COLUMNS
        );

        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setInt(1,  orderId);
        stmt.setInt(2,  orderItem.getFood().getId());
        stmt.setInt(3,  orderItem.getCount());

        return stmt;
    }

    private FoodItem convertResultSetToObject(ResultSet resultSet, Food food) throws SQLException {
        Integer count = resultSet.getInt("count");
        return new FoodItem(food, count);
    }
}
