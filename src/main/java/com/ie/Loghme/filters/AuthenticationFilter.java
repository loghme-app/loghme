package com.ie.Loghme.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.ie.Loghme.mappers.UserMapper;
import com.ie.Loghme.models.User;
import com.ie.Loghme.utils.Result;
import org.springframework.http.HttpStatus;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebFilter(filterName = "AuthenticationFilter")
public class AuthenticationFilter implements Filter {

    private JWTVerifier jwtVerifier;
    private UserMapper userMapper;
    private List<String> excludeURLs;

    public void init(FilterConfig filterConfig){
        excludeURLs = Arrays.asList(filterConfig.getInitParameter("exclude-urls").split(","));
        userMapper = UserMapper.getInstance();
        Algorithm algorithm = Algorithm.HMAC256("loghme");
        jwtVerifier = JWT.require(algorithm)
                .withIssuer("Loghme")
                .build();
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        System.out.println("-----------------------");
        System.out.println("AuthenticationFilter HTTP Request: ");
        System.out.println(request.getMethod());
        System.out.println(request.getRequestURI());

        if(excludeURLs.contains(request.getRequestURI())){
            chain.doFilter(servletRequest, servletResponse);
            return;
        }

        String token = request.getHeader("Authorization");
        if(token == null){
            this.setErrorResponse(response, "NO_JWT_TOKEN", HttpStatus.UNAUTHORIZED);
            return;
        }

        try{
            DecodedJWT jwt = jwtVerifier.verify(token);
            Integer userId = jwt.getClaim("userId").asInt();
            User user = userMapper.find(userId);
            request.setAttribute("user", user);
        }
        catch(JWTVerificationException exception){
            this.setErrorResponse(response, "INVALID_JWT_TOKEN", HttpStatus.FORBIDDEN);
            return;
        }

        chain.doFilter(servletRequest, servletResponse);
    }

    public void destroy(){}

    private void setErrorResponse(HttpServletResponse response, String error, HttpStatus status) throws IOException {
        Result result = new Result(false, error);
        response.getOutputStream().write(result.serialize());
        response.setHeader("Content-Type", "application/json");
        response.setStatus(status.value());
    }
}
