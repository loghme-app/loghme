package com.ie.Loghme.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.ie.Loghme.exceptions.*;
import com.ie.Loghme.models.User;
import com.ie.Loghme.services.UserService;
import com.ie.Loghme.utils.JwtToken;
import com.ie.Loghme.utils.Result;
import com.ie.Loghme.views.UserView;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    private UserService userService = UserService.getInstance();

    @JsonView(UserView.Summary.class)
    @RequestMapping(value = "user", method = RequestMethod.GET
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    public User getUser(@RequestAttribute(value="user") User user) {
        return user;
    }

    @RequestMapping(value = "credit", method = RequestMethod.PUT
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    public Result updateUserCredit( @RequestAttribute(value="user") User user,  @RequestParam(value = "amount") Integer amount) throws InvalidCreditAmount {

        userService.chargeCredit(user, amount);
        return new Result(true, null);
    }

    @RequestMapping(value = "register", method = RequestMethod.POST
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    public Result register(
            @RequestParam(value = "first_name") String firstName,
            @RequestParam(value = "last_name") String lastName,
            @RequestParam(value = "email") String email,
            @RequestParam(value = "password") String password,
            @RequestParam(value = "phone") String phone) throws EmailAlreadyExists, InvalidEmailAddress, WeakPassword {

        userService.registerUser(firstName, lastName, email, password, phone);
        return new Result(true, null);
    }

    @RequestMapping(value = "login", method = RequestMethod.POST
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    public JwtToken login(@RequestParam(value = "email") String email, @RequestParam(value = "password") String password)
            throws InvalidLoginInfo, UserNotFound {

        Integer userId = userService.checkUserLoginInfo(email, password);
        String jwt = userService.createJWT(userId);
        return new JwtToken(jwt);
    }

    @RequestMapping(value = "login/google", method = RequestMethod.POST
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    public JwtToken googleLogin(@RequestParam(value =  "google_token") String googleToken)
            throws InvalidGoogleLoginInfo, UserNotFound {

        Integer userId = userService.checkUserLoginInfo(googleToken);
        String jwt = userService.createJWT(userId);
        return new JwtToken(jwt);
    }

    @RequestMapping(value = "token", method = RequestMethod.GET
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    public Result validateToken() {
        return new Result(true, null);
    }

    @ExceptionHandler({InvalidCreditAmount.class, UserNotFound.class, InvalidEmailAddress.class,
            EmailAlreadyExists.class, WeakPassword.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    Result badRequestExceptionHandler(Exception exception) {
        return new Result(false, exception.getMessage());
    }

    @ExceptionHandler({InvalidLoginInfo.class, InvalidGoogleLoginInfo.class})
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    public @ResponseBody
    Result forbiddenExceptionHandler(Exception exception) {
        return new Result(false, exception.getMessage());
    }
}