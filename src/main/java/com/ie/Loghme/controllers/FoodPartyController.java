package com.ie.Loghme.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.ie.Loghme.exceptions.FoodNotFound;
import com.ie.Loghme.models.DiscountFood;
import com.ie.Loghme.services.FoodPartyService;
import com.ie.Loghme.utils.Result;
import com.ie.Loghme.views.DiscountFoodView;
import com.ie.Loghme.views.FoodPartyTimeView;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;

@RestController
public class FoodPartyController {
    private FoodPartyService foodPartyService = FoodPartyService.getInstance();

    @JsonView({DiscountFoodView.Summary.class})
    @RequestMapping(value = "discountFoods", method = RequestMethod.GET
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<DiscountFood> getDiscountFoods() {
        return foodPartyService.getFoods();
    }

    @RequestMapping(value = "discountFoods/time", method = RequestMethod.GET
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    public FoodPartyTimeView getRemainingTime() {
        Integer remainingTime = foodPartyService.getOffersRemainingTimeInSeconds();
        return new FoodPartyTimeView(remainingTime);
    }

    @JsonView({DiscountFoodView.Details.class})
    @RequestMapping(value = "discountFoods/{id}", method = RequestMethod.GET
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    public DiscountFood getDiscountFood(@PathVariable(value = "id") Integer id) throws FoodNotFound {
        return foodPartyService.getFood(id);
    }

    @ExceptionHandler({FoodNotFound.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public @ResponseBody
    Result notFoundExceptionHandler(Exception exception) {
        return new Result(false, exception.getMessage());
    }
}
