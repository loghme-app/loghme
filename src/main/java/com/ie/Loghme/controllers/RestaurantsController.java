package com.ie.Loghme.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.ie.Loghme.exceptions.FoodNotFound;
import com.ie.Loghme.exceptions.RestaurantNotFound;
import com.ie.Loghme.exceptions.RestaurantOutOfReach;
import com.ie.Loghme.models.Location;
import com.ie.Loghme.models.Restaurant;
import com.ie.Loghme.models.Food;
import com.ie.Loghme.models.User;
import com.ie.Loghme.services.RestaurantService;
import com.ie.Loghme.services.UserService;
import com.ie.Loghme.utils.Result;
import com.ie.Loghme.views.RestaurantView;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class RestaurantsController {
    private UserService userService = UserService.getInstance();
    private RestaurantService restaurantService = RestaurantService.getInstance();

    @JsonView({RestaurantView.Summary.class})
    @RequestMapping(value = "restaurants", method = RequestMethod.GET
    ,produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Restaurant> getRestaurants(
            @RequestAttribute(value="user") User user,
            @RequestParam(value = "page_num") Integer pageNum,
            @RequestParam(value = "items_per_page") Integer itemsPerPage) {

        return restaurantService.getRestaurantsForLocation(user, pageNum, itemsPerPage);
    }

    @JsonView({RestaurantView.Details.class})
    @RequestMapping(value = "restaurants/{id}", method = RequestMethod.GET
    ,produces = MediaType.APPLICATION_JSON_VALUE )
    public Restaurant getRestaurant(@RequestAttribute(value="user") User user, @PathVariable(value = "id") Integer id) throws RestaurantNotFound, RestaurantOutOfReach {

        return restaurantService.getRestaurant(id, user);
    }

    @RequestMapping(value = "restaurants/{r_id}/foods/{f_id}", method = RequestMethod.GET
            ,produces = MediaType.APPLICATION_JSON_VALUE )
    public Food getFoodFromRestaurant(@RequestAttribute(value="user") User user, @PathVariable(value = "r_id") Integer r_id, @PathVariable(value = "f_id") Integer f_id )
            throws RestaurantNotFound, RestaurantOutOfReach, FoodNotFound {

        return restaurantService.getFoodFromRestaurant(f_id, r_id, user);
    }

    @JsonView({RestaurantView.Summary.class})
    @RequestMapping(value = "search", method = RequestMethod.GET
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Restaurant> searchRestaurants(
            @RequestAttribute(value="user") User user,
            @RequestParam(value = "restaurant_keyword") String restaurantKeyword,
            @RequestParam(value = "food_keyword") String foodKeyword) {

        return restaurantService.searchRestaurants(restaurantKeyword, foodKeyword, user);
    }

    @ExceptionHandler({RestaurantNotFound.class, FoodNotFound.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public @ResponseBody
    Result notFoundExceptionHandler(Exception exception) {
        return new Result(false, exception.getMessage());
    }

    @ExceptionHandler({RestaurantOutOfReach.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody
    Result badRequestExceptionHandler(Exception exception) {
        return new Result(false, exception.getMessage());
    }

}