package com.ie.Loghme.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.ie.Loghme.exceptions.*;
import com.ie.Loghme.mappers.CartMapper;
import com.ie.Loghme.mappers.OrderMapper;
import com.ie.Loghme.mappers.UserMapper;
import com.ie.Loghme.models.Cart;
import com.ie.Loghme.models.Location;
import com.ie.Loghme.models.Order;
import com.ie.Loghme.models.User;
import com.ie.Loghme.services.OrderService;
import com.ie.Loghme.services.UserService;
import com.ie.Loghme.utils.Result;
import com.ie.Loghme.views.CartView;
import com.ie.Loghme.views.OrderView;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class OrdersController {

    private OrderService orderService = OrderService.getInstance();
    private UserService userService = UserService.getInstance();

    @JsonView(OrderView.Summary.class)
    @RequestMapping(value = "orders", method = RequestMethod.GET
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    public ArrayList<Order> getOrders(@RequestAttribute(value="user") User user) {
        return orderService.getOrdersOfUser(user);
    }

    @JsonView(OrderView.Details.class)
    @RequestMapping(value = "orders/{id}", method = RequestMethod.GET
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    public Order getOrder(@RequestAttribute(value="user") User user, @PathVariable(value = "id") Integer id) throws OrderNotFound {
        return orderService.getOrderById(user, id);
    }

    @RequestMapping(value = "orders", method = RequestMethod.POST)
    public Result finalizeOrder(@RequestAttribute(value="user") User user) throws NoItemInCartToFinalize, NotEnoughCreditForOrder, FoodOutOfStock {
        orderService.finalizeOrder(user);
        return new Result(true);
    }

    @JsonView(CartView.Details.class)
    @RequestMapping(value = "cart", method = RequestMethod.GET)
    public Cart getCart(@RequestAttribute(value="user") User user) {
        return userService.getCart(user);
    }

    @RequestMapping(value = "cart", method = RequestMethod.PUT)
    public Result addToCart(
            @RequestAttribute(value="user") User user,
            @RequestParam(value = "restaurantId") Integer restaurantId,
            @RequestParam(value = "foodId") Integer foodId,
            @RequestParam(value = "foodParty") boolean foodParty,
            @RequestParam(value = "count") Integer count)  throws RestaurantNotFound, FoodNotFound,
            RestaurantOutOfReach, OrderFromMultipleRestaurants, OrderFromDiscountAndNormalFood, FoodOutOfStock {

        if(foodParty){
            orderService.addDiscountFoodToUserCart(foodId, count, restaurantId, user);
        }
        else{
            orderService.addFoodToUserCart(foodId, count, restaurantId, user);
        }
        return new Result(true);
    }

    @RequestMapping(value = "cart", method = RequestMethod.DELETE)
    public Result removeFromCart(
            @RequestAttribute(value="user") User user,
            @RequestParam(value = "restaurantId") Integer restaurantId,
            @RequestParam(value = "foodId") Integer foodId ) throws
            RestaurantNotFound, FoodNotFound, FoodItemNotInCart {

        orderService.removeFoodFromUserCart(user, foodId, restaurantId);
        return new Result(true);
    }

    @ExceptionHandler({OrderNotFound.class, RestaurantNotFound.class, FoodNotFound.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public @ResponseBody Result NotFoundExceptionHandler(Exception exception) {
        return new Result(false, exception.getMessage());
    }

    @ExceptionHandler({NotEnoughCreditForOrder.class, NoItemInCartToFinalize.class, FoodOutOfStock.class, FoodItemNotInCart.class,
                       OrderFromDiscountAndNormalFood.class, OrderFromMultipleRestaurants.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody Result badRequestExceptionHandler(Exception exception) {
        return new Result(false, exception.getMessage());
    }

    @ExceptionHandler(RestaurantOutOfReach.class)
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    public @ResponseBody Result forbiddenExceptionHandler(Exception exception) {
        return new Result(false, exception.getMessage());
    }
}
