package com.ie.Loghme.models;

import com.fasterxml.jackson.annotation.*;
import com.ie.Loghme.views.CartView;
import com.ie.Loghme.views.DiscountFoodView;
import com.ie.Loghme.views.OrderView;
import com.ie.Loghme.views.RestaurantView;


public class Food {
    @JsonView({RestaurantView.Details.class, DiscountFoodView.Summary.class, CartView.Details.class})
    private Integer id;
    @JsonView({RestaurantView.Details.class, DiscountFoodView.Summary.class, OrderView.Details.class, CartView.Details.class})
    private String name;
    @JsonView(DiscountFoodView.Details.class)
    private String description;
    @JsonView({RestaurantView.Details.class, DiscountFoodView.Summary.class})
    private Double popularity;
    @JsonView({RestaurantView.Details.class, DiscountFoodView.Summary.class, OrderView.Details.class, CartView.Details.class})
    private Integer price;
    @JsonView({RestaurantView.Details.class, DiscountFoodView.Summary.class})
    private String imageURL;

    @JsonView(DiscountFoodView.Summary.class)
    @JacksonInject
    private String restaurantName;

    @JsonCreator
    public Food(
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("popularity") Double popularity,
            @JsonProperty("price") Integer price,
            @JsonProperty("image") String imageURL,
            @JsonProperty("restaurantName") String restaurantName
    ){
        this.name = name;
        this.description = description;
        this.popularity = popularity;
        this.price = price;
        this.imageURL = imageURL;
        this.restaurantName = restaurantName;
        this.id = 0;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Double getPopularity() {
        return popularity;
    }

    public Integer getPrice() {
        return price;
    }

    @JsonGetter("image")
    public String getImageURL() { return imageURL; }

    public Integer getId() { return id; }

    public String getRestaurantName() { return restaurantName; }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonSetter("image")
    public void setImageURL(String imageURL) { this.imageURL = imageURL; }

    public void setId(Integer id) { this.id = id; }

    public void setRestaurantName(String restaurantName) { this.restaurantName = restaurantName; }

}
