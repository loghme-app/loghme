package com.ie.Loghme.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.ie.Loghme.views.CartView;
import com.ie.Loghme.views.OrderView;

public class FoodItem {

    @JsonView({OrderView.Details.class, CartView.Details.class})
    Food food;
    @JsonView({OrderView.Details.class, CartView.Details.class})
    Integer count;

    public FoodItem(Food food, Integer count) {
        this.food = food;
        this.count = count;
    }

    public Food getFood(){
        return food;
    }

    public void setFood(Food food){
        this.food = food;
    }

    public Integer getCount(){
        return count;
    }

    public void setCount(Integer count){
        this.count = count;
    }

    public void changeCount(int amount){
        count += amount;
    }

}
