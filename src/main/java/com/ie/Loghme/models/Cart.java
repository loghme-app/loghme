package com.ie.Loghme.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.ie.Loghme.exceptions.FoodItemNotInCart;
import com.ie.Loghme.exceptions.FoodNotFound;
import com.ie.Loghme.exceptions.OrderFromDiscountAndNormalFood;
import com.ie.Loghme.exceptions.OrderFromMultipleRestaurants;
import com.ie.Loghme.mappers.DiscountFoodMapper;
import com.ie.Loghme.views.CartView;

import java.util.ArrayList;

public class Cart {
    @JsonView(CartView.Details.class)
    //TODO: remove this field
    private ArrayList<FoodItem> items = new ArrayList<>();
    private String restaurantName;
    @JsonView(CartView.Details.class)
    private Integer restaurantId;
    @JsonView(CartView.Details.class)
    private OrderType orderType;
    @JsonView(CartView.Details.class)
    private Integer totalPrice = 0;
    private Integer userId = 0;

    public Cart(Integer userId) {
        this.userId = userId;
    }

    public void addFood(Food food, Integer count, Integer restaurantId) throws OrderFromMultipleRestaurants, OrderFromDiscountAndNormalFood {
        if(items.isEmpty()){
            this.restaurantId = restaurantId;
            this.restaurantName = food.getRestaurantName();
            this.orderType = getFoodType(food);
        }
        if(!restaurantId.equals(this.restaurantId)){
            throw new OrderFromMultipleRestaurants();
        }
        if(!orderType.equals(getFoodType(food))){
            throw new OrderFromDiscountAndNormalFood();
        }
        for(FoodItem item: items){
            if(item.getFood().getId().equals(food.getId())) {
                item.changeCount(count);
                this.calculatePrice();
                return;
            }
        }
        items.add(new FoodItem(food,count));
        this.calculatePrice();
    }

    public void removeFood(Food food) throws FoodItemNotInCart {
        for(FoodItem item: items){
            if(item.getFood().getId().equals(food.getId())){
                if(item.getCount() > 1)
                    item.changeCount(-1);
                else
                    items.remove(item);
                this.calculatePrice();
                return;
            }
        }
        throw new FoodItemNotInCart();
    }


    private OrderType getFoodType(Food food){
        if(food instanceof DiscountFood){
            return OrderType.discount;
        }
        else{
            return OrderType.normal;
        }
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public Integer getTotalPrice(){
        return totalPrice;
    }

    private void calculatePrice(){
        int price = 0;
        for(FoodItem item : items){
            Integer unitPrice = item.getFood().getPrice();
            Integer count = item.getCount();
            price += unitPrice * count;
        }
        totalPrice = price;
    }

    public Integer getUserId() {
        return userId;
    }

    public ArrayList<FoodItem> getItems(){
        //TODO: create new ArrayList from list(CartItem(foodId,count)) (instead of updating)
        if((items.size()>0) && orderType.equals(OrderType.discount)){
            for(FoodItem item: this.items){
                try{
                    item.setFood(DiscountFoodMapper.getInstance().find(item.getFood().getId()));
                }
                catch(FoodNotFound e){
                    e.printStackTrace();
                }
            }
        }
        return items;
    }

    public void empty(){
        items.clear();
        totalPrice = 0;
    }

    @JsonIgnore
    public boolean isEmpty() { return items.isEmpty(); }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void emptyIfContainsDiscountFood(){
        if(orderType==OrderType.discount){
            this.empty();
        }
    }

}
