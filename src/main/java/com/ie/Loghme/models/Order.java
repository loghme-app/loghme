package com.ie.Loghme.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.ie.Loghme.mappers.OrderMapper;
import com.ie.Loghme.services.DeliveryService;
import com.ie.Loghme.views.OrderView;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Order {
    @JsonView(OrderView.Summary.class)
    private Integer id;
    @JsonView(OrderView.Details.class)
    private ArrayList<FoodItem> foods;
    @JsonView(OrderView.Details.class)
    private Integer price;
    @JsonView({OrderView.Summary.class, OrderView.Details.class})
    private String restaurantName;
    @JsonView(OrderView.Summary.class)
    private OrderStatus status;
    private Delivery delivery;
    private Integer deliveryTime;
    private Instant startDeliveryTime;
    private Integer userId;

    public Order(Cart cart) {
        this.foods = new ArrayList<>(cart.getItems());
        this.price = cart.getTotalPrice();
        this.id = 0;

        this.restaurantName = cart.getRestaurantName();
        this.status = OrderStatus.findingDelivery;
        this.userId = cart.getUserId();
    }

    public Order(Integer id, Integer price, String status, String restaurantName, Integer userId, ArrayList<FoodItem> items){
        this.price = price;
        this.id = id;
        if(status.equals("findingDelivery"))
            this.status = OrderStatus.findingDelivery;
        else if(status.equals("delivering"))
            this.status = OrderStatus.delivering;
        else
            this.status = OrderStatus.delivered;
        this.restaurantName = restaurantName;
        this.userId = userId;
        this.foods = new ArrayList<>(items);
    }

    public ArrayList<FoodItem> getFoods() {
        return foods;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getId() { return id; }

    public String getRestaurantName() {
        return restaurantName;
    }

    public OrderStatus getStatus() { return status; }

    public void setDelivery(Delivery delivery) { this.delivery = delivery; }

    public void findADelivery(Location userLocation){
        Timer timer = new Timer();
        TimerTask task = new GetDelivery(userLocation);
        timer.schedule(task,0, 30000);
    }

    public void setDeliveryTimer(Integer deliveryTime){
        startDeliveryTime = Instant.now();
        this.deliveryTime = deliveryTime;
        Timer timer = new Timer();
        TimerTask task = new SetOrderDelivered();
        timer.schedule(task,deliveryTime * 1000,1000);
    }

    public Integer getRemainedDeliveryTime(){
        Integer remainedTime = 0;
        if(status == OrderStatus.delivering) {
            Instant now = Instant.now();
            Duration timeElapsed = Duration.between(startDeliveryTime, now);
            remainedTime = deliveryTime - (int) (timeElapsed.toMillis() / 1000);
        }
        return remainedTime;
    }

    private class GetDelivery extends TimerTask {
        private Location userLocation;

        public GetDelivery(Location userLocation){
            super();
            this.userLocation = userLocation;
        }
        public void run(){
            if(DeliveryService.getInstance().setDeliveryForOrder(Order.this, this.userLocation))
                cancel();
        }
    }

    public void setStatus(OrderStatus status) { this.status = status; }

    private class SetOrderDelivered extends TimerTask {
        public void run(){
            Order.this.status = OrderStatus.delivered;
            OrderMapper.getInstance().update(Order.this);
            DeliveryService.getInstance().removeOrderForScheduling(Order.this);
            cancel();
        }
    }

    public Integer getDeliveryTime() {
        return deliveryTime;
    }

    public Instant getStartDeliveryTime() {
        return startDeliveryTime;
    }

    public Integer getUserId() {
        return userId;
    }
}
