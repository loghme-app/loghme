package com.ie.Loghme.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Location {
    private Integer x;
    private Integer y;

    @JsonCreator
    public Location(
            @JsonProperty("x") Integer x,
            @JsonProperty("y") Integer y
    ) {
        this.x = x;
        this.y = y;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }
}
