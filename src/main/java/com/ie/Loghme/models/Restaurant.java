package com.ie.Loghme.models;

import com.fasterxml.jackson.annotation.*;
import com.ie.Loghme.views.DiscountFoodView;
import com.ie.Loghme.views.RestaurantView;
import com.ie.Loghme.exceptions.FoodNotFound;

import java.util.ArrayList;


public class Restaurant {
    @JsonView({RestaurantView.Summary.class, DiscountFoodView.Details.class})
    private Integer id;
    @JsonView(RestaurantView.Summary.class)
    private String name;
    private Location location;
    @JsonView(RestaurantView.Summary.class)
    private String logoURL;
    @JsonView(RestaurantView.Details.class)
    private ArrayList<Food> menu;

    @JsonCreator
    public Restaurant(
            @JsonProperty("name") String name,
            @JsonProperty("location") Location location,
            @JsonProperty("logo") String logoURL,
            @JsonProperty("menu") ArrayList<Food> menu) {
        this.id = 0;
        this.name = name;
        this.location = location;
        this.logoURL = logoURL;
        this.menu = menu;
    }

    public Integer getId() { return id; }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public String getLogoURL() { return logoURL; }

    @JsonIgnore
    public void setId(Integer id) { this.id = id; }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @JsonSetter("logo")
    public void setLogoURL(String logoURL) { this.logoURL = logoURL; }

    public void setMenu(ArrayList<Food> menu) {
        this.menu = menu;
    }

    public ArrayList<Food> getMenu() {
        return menu;
    }

    public void addFoodToMenu(Food food) { menu.add(food); }

    public Food getFood(Integer foodId) throws FoodNotFound {
        for(Food food : menu){
            if(food.getId().equals(foodId))
                return food;
        }
        throw new FoodNotFound();
    }

}
