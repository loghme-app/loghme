package com.ie.Loghme.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.ie.Loghme.exceptions.OrderNotFound;
import com.ie.Loghme.mappers.OrderMapper;
import com.ie.Loghme.services.DeliveryService;
import com.ie.Loghme.views.UserView;

import java.util.ArrayList;

public class User {
    private Integer id;
    @JsonView(UserView.Summary.class)
    private String firstName;
    @JsonView(UserView.Summary.class)
    private String lastName;
    @JsonView(UserView.Summary.class)
    private String telephoneNumber;
    @JsonView(UserView.Summary.class)
    private String email;
    @JsonView(UserView.Summary.class)
    private Integer credit;
    private Location location;
    private String password;
    private ArrayList<Order> orders;

    public User(Integer id, String firstName, String lastName, String telephoneNumber,
                String email, Integer credit, Location location, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
        this.location = location;
        this.credit = credit;
        this.password = password;
        this.orders = new ArrayList<>();
    }

    public User(String firstName, String lastName, String telephoneNumber, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
        this.password = password;
        this.location = new Location(0,0);
        this.credit = 0;
        orders = new ArrayList<>();
    }

    public Integer getId() { return id; }

    public String getFirstName() { return firstName; }

    public String getLastName() { return lastName; }

    public String getTelephoneNumber() { return telephoneNumber; }

    public String getEmail() { return email; }

    public Integer getCredit() { return credit; }

    public Location getLocation() {
        return location;
    }

    public String getPassword() { return password; }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void submitCartAsOrder(Cart cart){
        Integer orderId = OrderMapper.getInstance().insert(new Order(cart));

        this.changeCredit(-cart.getTotalPrice());
        cart.empty();

        try{
            Order order = OrderMapper.getInstance().find(this.getId(), orderId);
            DeliveryService.getInstance().addOrderForScheduling(order, this.getLocation());
        }
        catch(OrderNotFound e){
            e.printStackTrace();
        }
    }

    public void setCredit(Integer credit) { this.credit = credit; }

    public void changeCredit(Integer chargedAmount){
        credit += chargedAmount;
    }

}
