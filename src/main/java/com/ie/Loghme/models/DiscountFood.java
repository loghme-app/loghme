package com.ie.Loghme.models;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.ie.Loghme.views.DiscountFoodView;

public class DiscountFood extends Food {
    @JsonView(DiscountFoodView.Summary.class)
    private Integer oldPrice;
    @JsonView(DiscountFoodView.Summary.class)
    private Integer count;

    @JsonView(DiscountFoodView.Details.class)
    @JacksonInject
    private Restaurant restaurant;

    @JsonCreator
    public DiscountFood(
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("popularity") Double popularity,
            @JsonProperty("price") Integer price,
            @JsonProperty("image") String imageURL,
            @JsonProperty("restaurantName") String restaurantName,
            @JsonProperty("oldPrice") Integer oldPrice,
            @JsonProperty("count") Integer count,
            @JsonProperty("restaurant") Restaurant restaurant
    ){
        super(name, description, popularity, price, imageURL, restaurantName);
        this.oldPrice = oldPrice;
        this.count = count;
        this.restaurant = restaurant;
    }

    public Integer getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(Integer oldPrice) {
        this.oldPrice = oldPrice;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public void changeCount(int count) {
        this.count += count;
    }

    public boolean isRemaining(int count) {
        return this.count - count >= 0;
    }
}
