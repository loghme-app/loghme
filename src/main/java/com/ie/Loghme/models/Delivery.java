package com.ie.Loghme.models;

public class Delivery {
    private String id;
    private Integer velocity;
    private Location location;

    public Integer getVelocity() {
        return velocity;
    }

    public Location getLocation() {
        return location;
    }

    public String getId() {
        return id;
    }
}
