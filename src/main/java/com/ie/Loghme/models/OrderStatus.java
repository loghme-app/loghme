package com.ie.Loghme.models;

public enum OrderStatus {
    findingDelivery,
    delivering,
    delivered
}
