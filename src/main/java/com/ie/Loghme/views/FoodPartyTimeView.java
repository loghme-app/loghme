package com.ie.Loghme.views;

public class FoodPartyTimeView {
    Integer remainingTime;

    public FoodPartyTimeView(Integer remainingTime) {
        this.remainingTime = remainingTime;
    }

    public Integer getRemainingTime() {
        return remainingTime;
    }
}
