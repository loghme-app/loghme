package com.ie.Loghme.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.ie.Loghme.exceptions.*;
import com.ie.Loghme.mappers.CartMapper;
import com.ie.Loghme.mappers.UserMapper;
import com.ie.Loghme.models.Cart;
import com.ie.Loghme.models.User;
import com.ie.Loghme.utils.GoogleTokenVerifier;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserService {
    private static UserService instance = null;
    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    private UserMapper userMapper = UserMapper.getInstance();
    private CartMapper cartMapper = CartMapper.getInstance();

    private UserService(){}

    public static UserService getInstance(){
        if(instance == null)
            instance = new UserService();
        return instance;
    }

    public void chargeCredit(User user, Integer credit) throws InvalidCreditAmount{
        if(credit<0){
            throw new InvalidCreditAmount();
        }
        user.changeCredit(credit);
        userMapper.update(user);
    }

    public Cart getCart(User user){
        return cartMapper.find(user.getId());
    }

    public Integer checkUserLoginInfo(String email, String password) throws UserNotFound, InvalidLoginInfo{
        User user = userMapper.findByEmail(email);
        if(! passwordEncoder.matches(password, user.getPassword()))
            throw new InvalidLoginInfo();
        return  user.getId();
    }

    public Integer checkUserLoginInfo(String googleToken) throws UserNotFound, InvalidGoogleLoginInfo {
        String email = GoogleTokenVerifier.getInstance().getEmail(googleToken);
        System.out.println("--------------------- EMAIL FROM GOOGLE ------------------------");
        System.out.println(email);
        User user = userMapper.findByEmail(email);
        return user.getId();
    }

    public String createJWT(Integer userId){
        String token = "";
        try {
            Algorithm algorithm = Algorithm.HMAC256("loghme");
            Date issuedAt = new Date();
            Date expiresAt = DateUtils.addDays(issuedAt, 1);

            token = JWT.create()
                    .withIssuer("Loghme")
                    .withIssuedAt(issuedAt)
                    .withExpiresAt(expiresAt)
                    .withClaim("userId", userId)
                    .sign(algorithm);
        } catch (JWTCreationException exception){
            exception.printStackTrace();
        }

        return token;
    }

    public void registerUser(String firstName, String lastName, String email, String password, String phone)
            throws InvalidEmailAddress, EmailAlreadyExists, WeakPassword {
        this.checkEmailFormat(email);
        this.checkPasswordStrength(password);

        User user = new User(firstName, lastName, phone, email, passwordEncoder.encode(password));
        userMapper.insert(user);
    }

    private void checkEmailFormat(String email) throws InvalidEmailAddress {
        String regex = "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        if(!matcher.matches())
            throw new InvalidEmailAddress();
    }

    private void checkPasswordStrength(String password) throws WeakPassword {
        if(password.length() < 4)
            throw new WeakPassword();
    }
}
