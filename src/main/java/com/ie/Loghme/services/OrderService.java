package com.ie.Loghme.services;

import com.ie.Loghme.exceptions.*;
import com.ie.Loghme.mappers.CartMapper;
import com.ie.Loghme.mappers.DiscountFoodMapper;
import com.ie.Loghme.mappers.OrderMapper;
import com.ie.Loghme.mappers.UserMapper;
import com.ie.Loghme.models.*;

import java.util.ArrayList;


public class OrderService {
    private static OrderService instance = null;
    private RestaurantService restaurantService;
    private FoodPartyService foodPartyService;
    private UserService userService;
    private CartMapper cartMapper;
    private OrderMapper orderMapper;
    private DiscountFoodMapper discountFoodMapper;
    private UserMapper userMapper;

    private OrderService(){
        restaurantService = RestaurantService.getInstance();
        foodPartyService = FoodPartyService.getInstance();
        userService = UserService.getInstance();
        cartMapper = CartMapper.getInstance();
        orderMapper = OrderMapper.getInstance();
        discountFoodMapper = DiscountFoodMapper.getInstance();
        userMapper = UserMapper.getInstance();
    }

    public static OrderService getInstance(){
        if(instance == null)
            instance = new OrderService();
        return instance;
    }

    public void addFoodToUserCart(Integer foodId, Integer count, Integer restaurantId, User user) throws
            RestaurantNotFound, FoodNotFound, RestaurantOutOfReach,
            OrderFromMultipleRestaurants, OrderFromDiscountAndNormalFood {

        Restaurant restaurant = restaurantService.getRestaurant(restaurantId, user);
        Food food = restaurant.getFood(foodId);

        cartMapper.find(user.getId()).addFood(food, count, restaurantId);
    }

    public void addDiscountFoodToUserCart(Integer foodId, Integer count, Integer restaurantId, User user) throws
            RestaurantNotFound, FoodNotFound, RestaurantOutOfReach, OrderFromMultipleRestaurants,
            OrderFromDiscountAndNormalFood, FoodOutOfStock {

        Restaurant restaurant = restaurantService.getRestaurant(restaurantId, user);
        DiscountFood food = foodPartyService.getFood(foodId);
        if(!food.isRemaining(count)){
            throw new FoodOutOfStock();
        }

        cartMapper.find(user.getId()).addFood(food, count, restaurantId);
    }

    public void removeFoodFromUserCart(User user, Integer foodId, Integer restaurantId) throws
            RestaurantNotFound, FoodNotFound, FoodItemNotInCart {

        Restaurant restaurant = restaurantService.getRestaurant(restaurantId);


        Cart cart = cartMapper.find(user.getId());
        if(cart.getOrderType().equals(OrderType.normal)){
            Food food = restaurant.getFood(foodId);
            cart.removeFood(food);
        }
        else{
            DiscountFood food = foodPartyService.getFood(foodId);
            cart.removeFood(food);
        }
    }

    public void finalizeOrder(User user) throws NoItemInCartToFinalize, NotEnoughCreditForOrder, FoodOutOfStock {
        Integer userId = user.getId();
        Cart userCart = cartMapper.find(userId);

        if(userCart.getItems().size()==0){
            throw new NoItemInCartToFinalize();
        }
        if(userCart.getOrderType().equals(OrderType.discount)){
            for(FoodItem item: userCart.getItems()){
                if(!(discountFoodMapper.findCount(item.getFood().getId()) >= item.getCount())){
                    throw new FoodOutOfStock();
                }
            }
        }
        Integer userCredit = user.getCredit();
        Integer orderPrice = userCart.getTotalPrice();
        if(userCredit < orderPrice){
            throw new NotEnoughCreditForOrder();
        }
        if(userCart.getOrderType() == OrderType.discount)
            foodPartyService.updateRemainingFoods(userCart);

        user.submitCartAsOrder(userCart);
        userMapper.update(user);
    }

    public Order getOrderById(User user, Integer orderId) throws OrderNotFound{
        return orderMapper.find(user.getId(), orderId);
    }

    public ArrayList<Order> getOrdersOfUser(User user){
        return orderMapper.findAll(user.getId());
    }
}