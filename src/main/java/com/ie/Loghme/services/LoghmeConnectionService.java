package com.ie.Loghme.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.InjectableValues;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ie.Loghme.mappers.DiscountFoodMapper;
import com.ie.Loghme.mappers.RestaurantMapper;
import com.ie.Loghme.models.*;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class LoghmeConnectionService{
    private static LoghmeConnectionService instance = null;

    private LoghmeConnectionService(){ }

    public static LoghmeConnectionService getInstance(){
        if(instance == null)
            instance = new LoghmeConnectionService();
        return instance;
    }

    public void getFoodPartyData(){
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet("http://138.197.181.131:8080/foodparty");
        try{
            CloseableHttpResponse response = httpClient.execute(request);

            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity);

            System.out.println("food party: " + result);
            updateFoodParty(result);

            httpClient.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void updateFoodParty(String json){
//        foodPartyRepo.empty();
        try {
            DiscountFoodMapper.getInstance().updateAll();
        }
        catch(SQLException e){
            e.printStackTrace();
        }

        try{
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonList = mapper.readTree(json);
            for(JsonNode jsonNode: jsonList){
                String restaurantName = jsonNode.path("name").asText();
                String menu = jsonNode.path("menu").toString();
                Integer restaurantId;


                ((ObjectNode)jsonNode).remove("id");
                ((ObjectNode)jsonNode).putArray("menu").removeAll();
                Restaurant restaurant = new ObjectMapper().readValue(jsonNode.toString(), Restaurant.class);
//                restaurantsRepo.addRestaurant(restaurant);
                try{
                    restaurantId = RestaurantMapper.getInstance().insert(restaurant);
                    if(restaurantId==-1){
                        restaurantId = RestaurantMapper.getInstance().findByName(restaurantName).getId();
                    }
                    restaurant.setId(restaurantId);


                    ObjectMapper foodMapper = new ObjectMapper();
                    InjectableValues.Std inject = new InjectableValues.Std();
                    inject.addValue(String.class, restaurantName);
                    inject.addValue(Restaurant.class, restaurant);
                    foodMapper.setInjectableValues(inject);

                    DiscountFoodMapper.getInstance().insertAll(Arrays.asList(foodMapper.readValue(menu, DiscountFood[].class)));
                }
                catch (SQLException e){
                    e.printStackTrace();
                }
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    public ArrayList<Delivery> getDeliveriesData(){
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet("http://138.197.181.131:8080/deliveries");
        ArrayList <Delivery> deliveries = new ArrayList<>();
        try{
            CloseableHttpResponse response = httpClient.execute(request);

            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity);

            System.out.println("deliveries: " + result);
            ObjectMapper mapper = new ObjectMapper();
            deliveries = mapper.readValue(result, new TypeReference<List<Delivery>>(){});

            httpClient.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return deliveries;
    }

    public void getRestaurantsData(){
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet("http://138.197.181.131:8080/restaurants");
        try{
            CloseableHttpResponse response = httpClient.execute(request);

            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity);

            System.out.println("restaurants: " + result);
            createRestaurants(result);

            httpClient.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void createRestaurants(String json){
        try{
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonList = mapper.readTree(json);
            for(JsonNode jsonNode: jsonList){
                ((ObjectNode)jsonNode).remove("id");
                String restaurantName = jsonNode.path("name").asText();
                InjectableValues inject = new InjectableValues.Std().addValue(String.class, restaurantName);
                Restaurant restaurant = new ObjectMapper().reader(inject)
                        .forType(Restaurant.class)
                        .readValue(jsonNode);


                RestaurantMapper.getInstance().insert(restaurant);

            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}