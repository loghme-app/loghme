package com.ie.Loghme.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ie.Loghme.exceptions.FoodNotFound;
import com.ie.Loghme.exceptions.RestaurantNotFound;
import com.ie.Loghme.exceptions.RestaurantOutOfReach;
import com.ie.Loghme.mappers.FoodMapper;
import com.ie.Loghme.mappers.RestaurantMapper;
import com.ie.Loghme.models.Food;
import com.ie.Loghme.models.Location;
import com.ie.Loghme.models.Restaurant;
import com.ie.Loghme.models.User;
import com.ie.Loghme.utils.LocationUtils;

import java.util.ArrayList;

public class RestaurantService {
    private static RestaurantService instance = null;

    private RestaurantMapper restaurantMapper = RestaurantMapper.getInstance();
    private LocationUtils locationUtils;

    private RestaurantService(){
        locationUtils = new LocationUtils();
    }

    public static RestaurantService getInstance(){
        if(instance == null)
            instance = new RestaurantService();
        return instance;
    }

    public ArrayList<Restaurant> getRestaurantsForLocation(User user, Integer pageNum, Integer itemsPerPage){
        return restaurantMapper.findAll(user.getLocation(), pageNum, itemsPerPage);
    }

    public Restaurant getRestaurantByName(String name) {
        return restaurantMapper.findByName(name);
    }

    public Restaurant getRestaurant(Integer id, User user) throws RestaurantNotFound, RestaurantOutOfReach {
        Restaurant restaurant = restaurantMapper.find(id);
        if(locationUtils.calculateDistance(user.getLocation(), restaurant.getLocation()) > 170)
            throw new RestaurantOutOfReach();
        return restaurant;
    }

    public Restaurant getRestaurant(Integer id) throws RestaurantNotFound {
        return restaurantMapper.find(id);
    }

    public Food getFoodFromRestaurant(Integer foodID, Integer restaurantID, User user) throws RestaurantNotFound, FoodNotFound, RestaurantOutOfReach {
        Restaurant restaurant = restaurantMapper.find(restaurantID);
        if(locationUtils.calculateDistance(user.getLocation(), restaurant.getLocation()) > 170)
            throw new RestaurantOutOfReach();
        return restaurant.getFood(foodID);
    }

    public ArrayList<Restaurant> searchRestaurants(String restaurantKeyword, String foodKeyword, User user){
        return restaurantMapper.search(restaurantKeyword, foodKeyword, user.getLocation());
    }
}