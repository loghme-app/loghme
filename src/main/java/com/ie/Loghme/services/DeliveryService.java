package com.ie.Loghme.services;

import com.ie.Loghme.mappers.DeliveryMapper;
import com.ie.Loghme.mappers.OrderMapper;
import com.ie.Loghme.models.*;
import com.ie.Loghme.utils.LocationUtils;

import java.util.ArrayList;

public class DeliveryService {
    private LocationUtils locationUtils;
    private static DeliveryService instance = null;
    private LoghmeConnectionService loghmeConnectionService;
    private UserService userService;
    private RestaurantService restaurantService;
    private ArrayList<Order> schedulingOrders;
    private DeliveryMapper deliveryMapper;
    private OrderMapper orderMapper;

    private final static int avgDeliverySearchTime = 60;
    private final static int avgDeliveryVelocity = 5;
    private final static double avgDeliveryDistanceToRestaurantRatio = 0.5;


    private DeliveryService(){
        locationUtils = new LocationUtils();
        loghmeConnectionService = LoghmeConnectionService.getInstance();
        userService = UserService.getInstance();
        restaurantService = RestaurantService.getInstance();
        schedulingOrders = new ArrayList<>();
        deliveryMapper = DeliveryMapper.getInstance();
        orderMapper = OrderMapper.getInstance();
    }

    public static DeliveryService getInstance(){
        if(instance == null)
            instance = new DeliveryService();
        return instance;
    }

    public void addOrderForScheduling(Order order, Location userLocation){
        schedulingOrders.add(order);
        order.findADelivery(userLocation);
    }

    public void removeOrderForScheduling(Order order){
        schedulingOrders.remove(order);
    }

    public Double calculateDeliveryTime(Location restaurantLoc, Location userLoc, Location deliveryLoc, Integer velocity){
        Double deliveryToRestaurantDist = locationUtils.calculateDistance(deliveryLoc, restaurantLoc);
        Double restaurantToUserDist = locationUtils.calculateDistance(restaurantLoc, userLoc);
        Double totalDist = deliveryToRestaurantDist + restaurantToUserDist;
        return (totalDist/velocity);
    }

    public Double estimateDeliveryTime(Location restaurantLoc, Location userLoc){
        Double restaurantToUserDist = locationUtils.calculateDistance(restaurantLoc, userLoc);
        Double deliveryToRestaurantDist = avgDeliveryDistanceToRestaurantRatio * restaurantToUserDist;
        Double totalDist = deliveryToRestaurantDist + restaurantToUserDist;

        return (avgDeliverySearchTime + (totalDist/avgDeliveryVelocity));
    }

    public Boolean setDeliveryForOrder(Order order, Location userLoc){
        ArrayList<Delivery> deliveries = loghmeConnectionService.getDeliveriesData();

        if(deliveries.isEmpty())
            return false;

        String restaurantName = order.getRestaurantName();
        Location restaurantLoc = restaurantService.getRestaurantByName(restaurantName).getLocation();

        Double bestDeliveryTime = Double.POSITIVE_INFINITY;
        Delivery bestDelivery = deliveries.get(0);

        for(Delivery currDelivery : deliveries){
            Double curDeliveryTime = calculateDeliveryTime(restaurantLoc, userLoc, currDelivery.getLocation(), currDelivery.getVelocity());
            if(bestDeliveryTime > curDeliveryTime){
                bestDeliveryTime = curDeliveryTime;
                bestDelivery = currDelivery;
            }
        }
        System.out.println(String.format("Set Delivery : %S to order with delivery time %d" ,bestDelivery.getId(), bestDeliveryTime.intValue()));
        order.setDelivery(bestDelivery);
        order.setDeliveryTimer(bestDeliveryTime.intValue());
        deliveryMapper.insert(bestDelivery, order.getId(), order.getDeliveryTime(), order.getStartDeliveryTime());
        order.setStatus(OrderStatus.delivering);
        orderMapper.update(order);
        return true;
    }


}
