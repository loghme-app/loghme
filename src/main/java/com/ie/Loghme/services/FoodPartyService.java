package com.ie.Loghme.services;

import com.ie.Loghme.exceptions.FoodNotFound;
import com.ie.Loghme.mappers.CartMapper;
import com.ie.Loghme.mappers.DiscountFoodMapper;
import com.ie.Loghme.models.Cart;
import com.ie.Loghme.models.DiscountFood;
import com.ie.Loghme.models.FoodItem;

import java.time.*;

import java.util.ArrayList;

public class FoodPartyService {
    private final int foodPartyDuration = 2 * 60;
    private static FoodPartyService instance = null;
    private DiscountFoodMapper discountFoodMapper = DiscountFoodMapper.getInstance();
    private Instant offerStartTime;


    private FoodPartyService(){ }

    public static FoodPartyService getInstance(){
        if(instance == null)
            instance = new FoodPartyService();
        return instance;
    }

    public ArrayList<DiscountFood> getFoods(){
        return discountFoodMapper.findAll();
    }

    public DiscountFood getFood(Integer foodId) throws FoodNotFound {
        return discountFoodMapper.find(foodId);
    }

    public void updateRemainingFoods(Cart userCart) {
        for(FoodItem item: userCart.getItems()){
            ((DiscountFood)item.getFood()).changeCount(-item.getCount());
            discountFoodMapper.update((DiscountFood)item.getFood());
        }
    }

    public void startNewOffer(){
        offerStartTime = Instant.now();
    }

    public int getOffersRemainingTimeInSeconds(){
        Instant now = Instant.now();
        return (int)(foodPartyDuration - (Duration.between(offerStartTime, now).toMillis() / 1000 ));
    }

    public void removeExpiredDiscountFoodsFromCarts(){
        CartMapper.getInstance().findAll().forEach(Cart::emptyIfContainsDiscountFood);
    }
}
